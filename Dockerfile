FROM quay.io/keycloak/keycloak:25.0.4 AS keycloak-builder

# default configurations
# database
ENV KC_DB=postgres
# feautures
ENV KC_FEATURES=admin-fine-grained-authz,token-exchange
# health
ENV KC_HEALTH_ENABLED=true
# metrics
ENV KC_METRICS_ENABLED=true

WORKDIR /opt/keycloak

RUN /opt/keycloak/bin/kc.sh --verbose build && \
    /opt/keycloak/bin/kc.sh show-config

FROM quay.io/keycloak/keycloak:25.0.4

WORKDIR /opt/keycloak

COPY --from=keycloak-builder /opt/keycloak/ /opt/keycloak/

ENTRYPOINT ["/opt/keycloak/bin/kc.sh", "start", "--optimized"]